'use strict';

var
  modelView = function modelView(spec) {
    var
      model = spec.model,
      render = spec.render;

    model.on('change', render);

    return spec;
  };

module.exports = modelView;

