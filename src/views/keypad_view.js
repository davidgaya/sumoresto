'use strict';

var
  $ = require('jquery-browserify'),
  Backbone = require('backbone'),

  KeypadView = Backbone.View.extend({
    className: "keypad",
    template: $("#keypad_template").html(),
    show: function () {
      this.$el.show();
    },
    hide: function () {
      this.$el.hide();
    },
    initialize: function (extras) {
      this.$el.html(this.template);
      this.$el.find("table").offset(extras.position);
      $("body").append(this.$el);
    },
    events: {
      "click td": "keyPressed",
      "click": "hide"
    },
    keyPressed: function (e) {
      var value = $(e.target).data('value');
      this.model.set({value: value});
    }
  }),

  keypadView = function (extensions) {
    return new KeypadView(extensions);
  };

module.exports = keypadView;
