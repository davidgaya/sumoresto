'use strict';

var
  keypadView = require('./keypad_view'),
  modelView = require('./model_view'),

  answerDigitView = function answerDigitView(spec) {
    var
      model = spec.model,
      $el = spec.el,
      keypad = keypadView({model: model, position: $el.position()}),
      render = function render() {
        $el.html(model.get('value'));
      };

    $el.on('click', function () { keypad.show(); });
    $el.addClass("answer");
    render();

    return modelView({model: model, render: render});
  };

module.exports = answerDigitView;
