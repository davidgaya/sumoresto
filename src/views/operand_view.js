'use strict';

var
  randomNumber = require('../models/random_number'),

  operandView = function operandView($this) {
    var number = randomNumber();
    $this.find(".ones").html(number.ones());
    $this.find(".tens").html(number.tens());
    $this.extend({
      value: function value() {
        return number.value();
      }
    });
    return $this;
  };

module.exports = operandView;
