'use strict';

var
  answerDigitView = require('./answer_digit_view'),

  totalView = function totalView(spec) {
    var
      $this = spec.el,
      model = spec.model;

    answerDigitView({el: $this.find(".ones"), model: model.ones});
    answerDigitView({el: $this.find(".tens"), model: model.tens});
    answerDigitView({el: $this.find(".hundreds"), model: model.hundreds});

    return spec;
  };

module.exports = totalView;
