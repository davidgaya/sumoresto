'use strict';

var
  modelView = require('./model_view'),

  audioWrapper = function audioWrapper(spec) {
    var audioDom = spec[0]; // we get DOM object to send multimedia messages that jQuery objects ignore
    return {
      play: function play() { audioDom.play(); },
      stop: function stop() { audioDom.pause(); audioDom.load(); }
    };
  },

  feedbackView = function feedbackView(spec) {
    var
      $el = spec.el,
      model = spec.model,
      audio = audioWrapper($el.find("audio")),
      render = function render() {
        if (model.isCorrect()) {
          audio.play();
          $el.removeClass('wrong').addClass('right');
        } else {
          audio.stop();
          $el.removeClass('right').addClass('wrong');
        }
      };

    return modelView({model: model, render: render});
  };

module.exports = feedbackView;
