'use strict';

var
  answerDigitView = require('./answer_digit_view'),
  digit = require('../models/digit'),

  carryingView = function carryingView(extensions) {
    var $this = extensions.el;

    answerDigitView({el: $this.find(".tens"), model: digit()});

    return $this;
  };

module.exports = carryingView;