'use strict';

var
  randomNumber = function () {
    var number = Math.floor(Math.random() * 99);
    return {
      value: function value() { return number; },
      ones: function ones() { return number % 10; },
      tens: function tens() { return Math.floor(number / 10)  % 10; },
      hundreds: function hundreds() { return Math.floor(number / 100)  % 10; }
    };
  };

module.exports = randomNumber;
