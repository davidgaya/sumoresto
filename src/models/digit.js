'use strict';

var
  Backbone = require('backbone'),

  Digit =  Backbone.Model.extend({
    defaults: function () {
      return {
        value: null
      };
    }
  }),

  digit = function () {
    return new Digit();
  };

module.exports = digit;
