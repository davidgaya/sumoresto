'use strict';

var
  Backbone = require('backbone'),
  digit = require('./digit'),

  ResultChecker = Backbone.Model.extend({
    initialize: function () {
      this.ones = digit();
      this.tens = digit();
      this.hundreds = digit();
      this.ones.on('change', function () { this.trigger('change'); }, this);
      this.tens.on('change', function () { this.trigger('change'); }, this);
      this.hundreds.on('change', function () { this.trigger('change'); }, this);
    },
    isCorrect: function () {
      var computedNumber = this.ones.get('value') + 10 * (this.tens.get('value')) + 100 * (this.hundreds.get('value'));
      return (this.get('number') === computedNumber);
    }
  }),

  resultChecker = function (extensions) {
    return new ResultChecker(extensions);
  };

module.exports = resultChecker;
