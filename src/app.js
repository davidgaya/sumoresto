'use strict';

var
  $ = require('jquery-browserify'),
  resultChecker = require('./models/result_checker'),
  operand = require('./views/operand_view'),
  carrying = require('./views/carrying_view'),
  total = require('./views/total_view'),
  feedback = require('./views/feedback_view'),

  app = function () {

    var
      first_number = operand($("#number_1")).value(),
      second_number = operand($("#number_2")).value(),
      result = resultChecker({number: first_number + second_number});

    carrying({el: $("#carrying")});
    total({el: $("#total"), model: result});
    feedback({el: $("#result"), model: result});

  };

module.exports = app;
